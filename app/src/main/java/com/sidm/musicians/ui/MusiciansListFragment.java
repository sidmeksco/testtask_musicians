package com.sidm.musicians.ui;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sidm.musicians.R;
import com.sidm.musicians.db.MusiciansDBHelper;
import com.sidm.musicians.engine.DownloadService;
import com.sidm.musicians.objects.Musician;
import com.sidm.musicians.ui.adapters.MusiciansListAdapter;
import com.sidm.musicians.ui.loaders.MusicianListLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusiciansListFragment extends Fragment implements MusiciansListAdapter.OnClickListener, MusiciansDBHelper.DatabaseUpdateListener {

    private OnMusiciansListActionListener listener;
    private RecyclerView rvMusicianList;
    private MusiciansListAdapter musiciansListAdapter;

    private static final int LOADER_ID = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            listener = (OnMusiciansListActionListener)getActivity();
        }catch (Exception e){
            throw new ClassCastException(getActivity().toString() + " must implement OnMusiciansListActionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.musicians_list_fragment_layout, container, false);
        rvMusicianList = (RecyclerView)view.findViewById(R.id.rv_list);

        List<Musician> data = new ArrayList<>();

        musiciansListAdapter = new MusiciansListAdapter(data, this);
        rvMusicianList.setAdapter(musiciansListAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvMusicianList.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, musicianListLoaderCallback);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.activity_title);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        MusiciansDBHelper.getInstance(getActivity()).unregisterListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        MusiciansDBHelper.getInstance(getActivity()).registerListener(this);
    }

    @Override
    public void onItemClick(Musician musician) {
        listener.onMusicianSelected(musician);
    }

    @Override
    public void onDatabaseUpdate() {
        getLoaderManager().restartLoader(LOADER_ID, null, musicianListLoaderCallback);
    }

    public interface OnMusiciansListActionListener{
        /**
         *
         * @param musician - selected
         */
        void onMusicianSelected(Musician musician);
    }

    private LoaderManager.LoaderCallbacks<List<Musician>> musicianListLoaderCallback = new LoaderManager.LoaderCallbacks<List<Musician>>() {
        @Override
        public Loader<List<Musician>> onCreateLoader(int id, Bundle args) {

            return new MusicianListLoader(getContext());
        }

        @Override
        public void onLoadFinished(Loader<List<Musician>> loader, List<Musician> data) {
            if(data != null){
                if(musiciansListAdapter == null){
                    musiciansListAdapter = new MusiciansListAdapter(data, MusiciansListFragment.this);
                }else{
                    musiciansListAdapter.update(data);
                }
            }
        }

        @Override
        public void onLoaderReset(Loader<List<Musician>> loader) {

        }
    };

}
