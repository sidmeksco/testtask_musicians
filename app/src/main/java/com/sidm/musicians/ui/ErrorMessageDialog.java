package com.sidm.musicians.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.sidm.musicians.R;

/**
 * Created by sidm on 05.05.2016.
 */
public class ErrorMessageDialog extends DialogFragment{

    public static final String DIALOG_TAG = "errorMessageDialoge";

    private String message;

    public ErrorMessageDialog setMessageText(String message){
        this.message = message;
        return this;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.error_dialog_title)
                .setMessage(message)
                .setNeutralButton(R.string.button_ok_title, null);


        return builder.create();
    }
}
