package com.sidm.musicians.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sidm.musicians.R;
import com.sidm.musicians.objects.Musician;
import com.sidm.musicians.util.Utils;

import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusiciansListAdapter extends RecyclerView.Adapter<MusiciansListAdapter.MusicianViewHolder>{

    private List<Musician> dataSet;
    private OnClickListener listener;
    private int imageViewHeight = 0;
    private int imageViewWidth = 0;

    /**
     *
     * @param musicians - dataset to show in list
     * @param listener - callback to send itemclick event
     */
    public MusiciansListAdapter(List<Musician> musicians, OnClickListener listener){
        this.dataSet = musicians;
        this.listener = listener;
    }


    @Override
    public boolean onFailedToRecycleView(MusicianViewHolder holder) {
        return true;
    }

    @Override
    public MusicianViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.musician_item, parent, false);
        MusicianViewHolder viewHolder = new MusicianViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MusicianViewHolder holder, int position) {
        final Musician musician = dataSet.get(position);
        holder.name.setText(musician.getName());
        holder.genres.setText(musician.getGenres());
        holder.statistics.setText(musician.getAlbumsRepresentation() + ", " + musician.getTracksRepresentation());
        if(imageViewHeight==0){
            imageViewHeight = holder.image.getHeight();
            imageViewWidth = holder.image.getWidth();
        }
        Utils.loadImage(holder.image, musician.getCoverSmall(), imageViewWidth, imageViewHeight);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(musician);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MusicianViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView genres;
        TextView statistics; //amount of albums and songs info
        ImageView image;

        MusicianViewHolder(View itemView){
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name);
            genres = (TextView)itemView.findViewById(R.id.genres);;
            statistics = (TextView)itemView.findViewById(R.id.statistics);
            image = (ImageView)itemView.findViewById(R.id.musician_image);
        }
    }

    /**
     * update adapter date
     *
     * @param musicianList - new list of musician
     */
    public void update(List<Musician> musicianList){
        this.dataSet = musicianList;
        notifyDataSetChanged();
    }

    public interface OnClickListener{
        void onItemClick(Musician musician);
    }
}
