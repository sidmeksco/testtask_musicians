package com.sidm.musicians.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sidm.musicians.R;
import com.sidm.musicians.engine.App;
import com.sidm.musicians.objects.Musician;
import com.sidm.musicians.util.Utils;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusicianDetailFragment extends Fragment {

    private Musician musician;

    private static final String KEY_MUSICIAN = "musiciandetailfragment.musician";

    private TextView tvGenres;
    private TextView tvAlbums;
    private TextView tvTracks;
    private TextView tvDescription;
    private ImageView ivImage;

    public static MusicianDetailFragment newInstance(Musician musician){
        MusicianDetailFragment fragment = new MusicianDetailFragment();
        fragment.musician = musician;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment_layout, container, false);
        tvGenres = (TextView)view.findViewById(R.id.genres);
        tvAlbums = (TextView)view.findViewById(R.id.albums);
        tvTracks = (TextView)view.findViewById(R.id.tracks);
        tvDescription = (TextView)view.findViewById(R.id.description);
        ivImage = (ImageView)view.findViewById(R.id.musician_image);
        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(musician.getLink() != null && !musician.getLink().isEmpty()){
                    try{
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(musician.getLink()));
                        startActivity(intent);
                    }catch(Exception e){
                        Log.i(App.TAG, "show musicain link error. musician_id="+musician.getId() + " link="+musician.getLink());
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_MUSICIAN, musician);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            musician = (Musician)savedInstanceState.getParcelable(KEY_MUSICIAN);
        };
        fillMusicianInfo();

    }

    private void fillMusicianInfo(){
        getActivity().setTitle(musician.getName());

        tvGenres.setText(musician.getGenres());
        tvAlbums.setText(musician.getAlbumsRepresentation());
        tvTracks.setText(musician.getTracksRepresentation());
        tvDescription.setText(musician.getDescription());

        Utils.loadImage(ivImage, musician.getCoverBig(), ivImage.getWidth(), ivImage.getHeight());
    }
}
