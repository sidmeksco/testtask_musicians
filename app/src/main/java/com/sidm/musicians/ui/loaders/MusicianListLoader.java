package com.sidm.musicians.ui.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.sidm.musicians.db.MusiciansDBHelper;
import com.sidm.musicians.objects.Musician;

import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusicianListLoader extends AsyncTaskLoader<List<Musician>> {

    private Context context;

    public MusicianListLoader(Context context){
        super(context);
        this.context = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }


    @Override
    public List<Musician> loadInBackground() {
        MusiciansDBHelper dbHelper = MusiciansDBHelper.getInstance(context);
        return dbHelper.getMusicians();
    }
}
