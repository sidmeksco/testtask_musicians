package com.sidm.musicians.ui;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sidm.musicians.R;
import com.sidm.musicians.engine.DownloadService;
import com.sidm.musicians.objects.Musician;

public class MainActivity extends AppCompatActivity implements MusiciansListFragment.OnMusiciansListActionListener, FragmentManager.OnBackStackChangedListener {

    private DownloadServiceErrorReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(savedInstanceState == null){
            MusiciansListFragment musiciansListFragment = new MusiciansListFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.frame_container, musiciansListFragment);
            transaction.commit();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(this);
        setUpButtonVisability();

    }

    @Override
    protected void onStart() {
        super.onStart();
        receiver = new DownloadServiceErrorReceiver();
        IntentFilter intentFilter = new IntentFilter(DownloadService.ACTION_DOWNLOADSERVICE);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onMusicianSelected(Musician musician) {
        MusicianDetailFragment detailFragment = MusicianDetailFragment.newInstance(musician);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_container, detailFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                getSupportFragmentManager().popBackStack();
                break;
            case R.id.update:
                Intent intent = new Intent(this, DownloadService.class);
                startService(intent);
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onBackStackChanged() {
        setUpButtonVisability();
    }

    private void setUpButtonVisability(){
        boolean visabilityValue = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(visabilityValue);
    }

    public class DownloadServiceErrorReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String errorMessage = intent.getStringExtra(DownloadService.KEY_DOWNLOADSERVICE);
            new ErrorMessageDialog().setMessageText(errorMessage).show(getSupportFragmentManager(), ErrorMessageDialog.DIALOG_TAG);
        }
    }


}
