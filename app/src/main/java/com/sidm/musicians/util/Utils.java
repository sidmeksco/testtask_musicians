package com.sidm.musicians.util;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

import com.sidm.musicians.R;
import com.sidm.musicians.engine.App;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;


/**
 * Created by sidm on 24.04.2016.
 */
public class Utils {

    /**
     * load image in target image view
     *
     * @param target - image view
     * @param imageURL - image url
     * @param imageWidth - nessesary image width (target image view size)
     * @param imageHeight - nessesary image height (target image view size)
     */
    public static void loadImage(final ImageView target, final String imageURL, final int imageWidth, final int imageHeight){
        if(imageURL == null || imageURL.isEmpty())
            return;

        RequestCreator requst = Picasso.with(target.getContext())
                .load(imageURL)
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image);
                if(imageHeight > 0 && imageWidth > 0){
                    requst.resize(imageWidth, imageHeight);
                }
                requst.networkPolicy(NetworkPolicy.OFFLINE)
                .into(target, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        RequestCreator requst = Picasso.with(target.getContext())
                                .load(imageURL)
                                .placeholder(R.drawable.no_image)
                                .error(R.drawable.no_image);
                            if(imageHeight > 0 && imageWidth > 0){
                                requst.resize(imageWidth, imageHeight);
                            }
                            requst.into(target);
                    }
                });
    }

    /**
     * check if network connection is avaliable
     *
     * @return
     */
    public static boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.getApp().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

}
