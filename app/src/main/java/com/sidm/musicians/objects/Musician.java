package com.sidm.musicians.objects;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.sidm.musicians.db.TableMusician;

/**
 * Created by sidm on 23.04.2016.
 */
public class Musician implements Parcelable {

    private long id;
    private String name;
    private String genres; //of course ideal way to keep genres info is a list, but because of we only show a line in UI string var will be used in order to simplyfy logic
    private int tracks; //number of tracks
    private int albums; //number of albums
    private String link;
    private String description;
    private String coverSmall; //link to small cover
    private String coverBig; //link to big cover

    public Musician(String name, String genres, int albums, int tracks){
        this.name = name;
        this.genres = genres;
        this.albums = albums;
        this.tracks = tracks;
    }

    public Musician(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public int getTracks() {
        return tracks;
    }

    public void setTracks(int tracks) {
        this.tracks = tracks;
    }

    public int getAlbums() {
        return albums;
    }

    public void setAlbums(int albums) {
        this.albums = albums;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverSmall() {
        return coverSmall;
    }

    public void setCoverSmall(String coverSmall) {
        this.coverSmall = coverSmall;
    }

    public String getCoverBig() {
        return coverBig;
    }

    public void setCoverBig(String coverBig) {
        this.coverBig = coverBig;
    }


    /**
     * return representation of albums quantity
     *
     * @return
     */
    public String getAlbumsRepresentation(){
        String representation = null;
        String strQuantity = String.valueOf(albums);
        if(albums > 4 && albums < 21){
            representation = albums + " альбомов";
        }else if(strQuantity.endsWith("1")){
            representation = albums + " альбом";
        }else if(strQuantity.endsWith("2") || strQuantity.endsWith("3") || strQuantity.endsWith("4")) {
            representation = albums + " альбома";
        }else{
            representation = albums + " альбомов";
        }
        return representation;
    }

    /**
     * return representation of traks quantity
     *
     * @return
     */
    public String getTracksRepresentation(){
        String representation = null;
        String strQuantity = String.valueOf(tracks);
        if(tracks > 4 && tracks < 21){
            representation = tracks + " трэков";
        }else if(strQuantity.endsWith("1")){
            representation = tracks + " трэк";
        }else if(strQuantity.endsWith("2") || strQuantity.endsWith("3") || strQuantity.endsWith("4")) {
            representation = tracks + " трэка";
        }else{
            representation = tracks + " трэков";
        }
        return representation;
    }

    /**
     * create musician object from database cursor
     *
     * @param cursor
     * @return
     */
    public static Musician createFromCursor(Cursor cursor){
        Musician musician = new Musician();
        musician.setId(cursor.getLong(cursor.getColumnIndex(TableMusician.COL_ID)));
        musician.setName(cursor.getString(cursor.getColumnIndex(TableMusician.COL_NAME)));
        musician.setGenres(cursor.getString(cursor.getColumnIndex(TableMusician.COL_GENRES)));
        musician.setTracks(cursor.getInt(cursor.getColumnIndex(TableMusician.COL_TRACKS)));
        musician.setAlbums(cursor.getInt(cursor.getColumnIndex(TableMusician.COL_ALBUMS)));
        musician.setLink(cursor.getString(cursor.getColumnIndex(TableMusician.COL_LINK)));
        musician.setDescription(cursor.getString(cursor.getColumnIndex(TableMusician.COL_DESCRIPTION)));
        musician.setCoverBig(cursor.getString(cursor.getColumnIndex(TableMusician.COL_COVER_BIG)));
        musician.setCoverSmall(cursor.getString(cursor.getColumnIndex(TableMusician.COL_COVER_SMALL)));

        return musician;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(genres);
        dest.writeInt(tracks);
        dest.writeInt(albums);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(coverSmall);
        dest.writeString(coverBig);
    }

    public static final Parcelable.Creator<Musician> CREATOR = new Parcelable.Creator<Musician>(){
        @Override
        public Musician createFromParcel(Parcel source) {
            Musician musician = new Musician();
            musician.setId(source.readLong());
            musician.setName(source.readString());
            musician.setGenres(source.readString());
            musician.setTracks(source.readInt());
            musician.setAlbums(source.readInt());
            musician.setLink(source.readString());
            musician.setDescription(source.readString());
            musician.setCoverSmall(source.readString());
            musician.setCoverBig(source.readString());

            return musician;
        }

        @Override
        public Musician[] newArray(int size) {
            return new Musician[0];
        }
    };
}
