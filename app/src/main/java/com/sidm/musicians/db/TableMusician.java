package com.sidm.musicians.db;

/**
 * Created by sidm on 23.04.2016.
 */
public class TableMusician {

    public static final String TABLE_NAME = "musicians";

    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_GENRES = "genres";
    public static final String COL_TRACKS = "tracks";
    public static final String COL_ALBUMS = "albums";
    public static final String COL_LINK = "link";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_COVER_BIG = "cover_big";
    public static final String COL_COVER_SMALL = "cover_small";


    /**
     *
     * @return return table creation script
     */
    public static String getCreationTableScript(){

        String script = "create table if not exists " + TABLE_NAME + " ("
                + COL_ID + " integer primary key autoincrement, "
                + COL_NAME + " text, "
                + COL_GENRES + " text, "
                + COL_TRACKS + " integer, "
                + COL_ALBUMS + " integer, "
                + COL_LINK + " text, "
                + COL_DESCRIPTION + " text, "
                + COL_COVER_SMALL + " text, "
                + COL_COVER_BIG + " text)";

        return script;
    }
}
