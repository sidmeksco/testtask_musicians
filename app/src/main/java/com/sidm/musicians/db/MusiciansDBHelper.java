package com.sidm.musicians.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sidm.musicians.objects.Musician;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusiciansDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "musicians.db";
    private static final int DATABASE_VER = 1;
    private static volatile MusiciansDBHelper instance;
    private Context context;
    private SQLiteDatabase database;
    private DatabaseUpdateListener listener; //because of app consist of only one fragment which is interested in db updates simple var for listener wil be used (not array)

    private MusiciansDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
        this.context = context;
        this.database = getWritableDatabase();
    }

    public static MusiciansDBHelper getInstance(Context context){
        MusiciansDBHelper localInstance = instance;
        if(localInstance == null){
            synchronized (MusiciansDBHelper.class){
                localInstance = instance;
                if(localInstance == null){
                    localInstance = instance = new MusiciansDBHelper(context, DATABASE_NAME, null, DATABASE_VER);
                }
            }
        }
        return localInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableMusician.getCreationTableScript());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * update musicians in db - replace old data with musicianList parameter. (do delete and insert)
     *
     * @param musicianList - list of musician which will be inserten in db
     */
    public void updateMusicianInfo(List<Musician> musicianList){
        ContentValues cv;
        database.beginTransaction();
        clearMusiciansTable();
        for(Musician musician : musicianList){
            cv = new ContentValues();
            cv.put(TableMusician.COL_ID, musician.getId());
            cv.put(TableMusician.COL_NAME, musician.getName());
            cv.put(TableMusician.COL_GENRES, musician.getGenres());
            cv.put(TableMusician.COL_TRACKS, musician.getTracks());
            cv.put(TableMusician.COL_ALBUMS, musician.getAlbums());
            cv.put(TableMusician.COL_LINK, musician.getLink());
            cv.put(TableMusician.COL_DESCRIPTION, musician.getDescription());
            cv.put(TableMusician.COL_COVER_BIG, musician.getCoverBig());
            cv.put(TableMusician.COL_COVER_SMALL, musician.getCoverSmall());

            this.database.insert(TableMusician.TABLE_NAME, null, cv);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        notifyDatabaseUpdated();

    }

    private void clearMusiciansTable(){
        database.delete(TableMusician.TABLE_NAME, null, null);
    }

    /**
     * get list of usician from DB
     *
     * @return
     */
    public List<Musician> getMusicians(){
        String querry = "select * from " + TableMusician.TABLE_NAME + " order by " + TableMusician.COL_ID;

        Cursor resultCursor = database.rawQuery(querry, null);
        List<Musician> musicianList = new ArrayList<>(resultCursor.getCount());

        while(resultCursor.moveToNext()){
            musicianList.add(Musician.createFromCursor(resultCursor));
        }

        return musicianList;
    }

    /**
     * get musician from DB by id
     *
     * @param id - musician ID
     *
     * @return
     */
    public Musician getMusicianByID(long id){
        String querry = "select * from " + TableMusician.TABLE_NAME + " where " + TableMusician.COL_ID + " = " + id;
        Musician musician = null;
        Cursor cursor = database.rawQuery(querry, null);
        if(cursor.getCount() == 1 && cursor.moveToNext()){
            musician = Musician.createFromCursor(cursor);
        }

        return musician;
    }

    public interface DatabaseUpdateListener{
        void onDatabaseUpdate();
    }

    public void registerListener(DatabaseUpdateListener listener){
        this.listener = listener;
    }

    public void unregisterListener(){
        listener = null;
    }

    private void notifyDatabaseUpdated(){
        if(listener != null){
            listener.onDatabaseUpdate();
        }
    }

}
