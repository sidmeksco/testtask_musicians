package com.sidm.musicians.engine;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by sidm on 23.04.2016.
 */
public class NetworkCommunicator {

    public static final String link = "http://cache-spb09.cdn.yandex.net/download.cdn.yandex.net/mobilization-2016/artists.json";

    public static String sendRequest(String link) throws Exception {
        String response = null;

        HttpURLConnection connection = null;
        URL url = new URL(link);

        try{
            connection = (HttpURLConnection)url.openConnection();
           // prepareConnection(connection);

            int responceCode = connection.getResponseCode();
            if(responceCode == HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuilder sbResult = new StringBuilder();
                while((line = reader.readLine()) != null){
                    sbResult.append(line);
                }
                response = sbResult.toString();
            }else{

            }

        }catch (Exception e){
            Log.e(App.TAG, "network exception ");
            e.printStackTrace();
            throw e;
        }finally {
            if(connection != null){
                connection.disconnect();
            }
        }

        return response;
    }

    private static void prepareConnection(HttpURLConnection connection) throws Exception{
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);
        connection.setDoInput(true);
    }
}
