package com.sidm.musicians.engine;

import android.util.JsonReader;
import android.util.Log;

import com.sidm.musicians.objects.Musician;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class MusiciansJSONParser {

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_GENRES = "genres";
    private static final String KEY_TRACKS = "tracks";
    private static final String KEY_ALBUMS = "albums";
    private static final String KEY_LINK = "link";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_COVER = "cover";
    private static final String KEY_COVER_SMALL = "small";
    private static final String KEY_COVER_BIG = "big";

    /**
     * parse input musician data stream
     *
     * @param inputStream - json data stream
     *
     * @return list of parsed musician
     */
    public static List<Musician> parseMusicianData(InputStream inputStream) throws Exception{
        JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
        List<Musician> musicianList = new ArrayList<>();

        try{
            parseMusiciansData(reader, musicianList);
        }catch(Exception e){
            Log.e(App.TAG, "json parser error");
            e.printStackTrace();
            throw e;
        }

        return musicianList;
    }

    /**
     * parse input musician string
     *
     * @param inputJSONString
     *
     * @return list of parsed musician
     */
    public static List<Musician> parseMusicianData(String inputJSONString) throws Exception{
        return parseMusicianData(new ByteArrayInputStream(inputJSONString.getBytes()));
    }

    private static void parseMusiciansData(JsonReader reader, List<Musician> musicianList) throws Exception{
        reader.beginArray();
        while(reader.hasNext()){
            Musician musician = parseMusicianItem(reader);
            musicianList.add(musician);
        }
        reader.endArray();
    }

    private static Musician parseMusicianItem(JsonReader reader) throws Exception{
        Musician musician = new Musician();
        reader.beginObject();
        while(reader.hasNext()){
            String name = reader.nextName();
            if(name.equals(KEY_ID)){
                musician.setId(reader.nextLong());
                //Log.i(App.TAG, "=======key " + musician.getId());
            }else if(name.equals(KEY_NAME)){
                musician.setName(reader.nextString());
            }else if(name.equals(KEY_GENRES)){
                musician.setGenres(parseGenres(reader));
            }else if(name.equals(KEY_TRACKS)){
                musician.setTracks(reader.nextInt());
            }else if(name.equals(KEY_ALBUMS)){
                musician.setAlbums(reader.nextInt());
            }else if(name.equals(KEY_LINK)){
                musician.setLink(reader.nextString());
            }else if(name.equals(KEY_DESCRIPTION)){
                musician.setDescription(reader.nextString());
            }else if(name.equals(KEY_COVER)){
                parseCover(reader, musician);
            }else{
                reader.skipValue();
            }
        }

        reader.endObject();
        return musician;
    }

    //
    //instead of array use string for ganres in order to simplify code
    private static String parseGenres(JsonReader reader) throws Exception{
        StringBuilder genres = new StringBuilder();
        reader.beginArray();
        while(reader.hasNext()){
            genres.append(reader.nextString() + ", ");
        }
        reader.endArray();
        if(genres.length() > 2){
            genres.substring(0, genres.length()-2);
        }
        return genres.toString();
    }

    private static void parseCover(JsonReader reader, Musician musician) throws Exception{
        reader.beginObject();
        while(reader.hasNext()){
            String name = reader.nextName();
            if(name.equals(KEY_COVER_SMALL)){
                musician.setCoverSmall(reader.nextString());
            }else if(name.equals(KEY_COVER_BIG)){
                musician.setCoverBig(reader.nextString());
            }else{
                reader.skipValue();
            }
        }
        reader.endObject();
    }
}
