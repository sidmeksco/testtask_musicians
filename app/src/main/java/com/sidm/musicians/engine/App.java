package com.sidm.musicians.engine;

import android.app.Application;
import android.content.Intent;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by sidm on 24.04.2016.
 */
public class App extends Application {

    private static App app;
    public static final String TAG = "com.sidm.musicians";

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        configurePicasso();
        Intent intent = new Intent(this, DownloadService.class);
        startService(intent);

    }

    public static App getApp(){
        return app;
    }

    private void configurePicasso(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        //built.setIndicatorsEnabled(true);
        //built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }
}
