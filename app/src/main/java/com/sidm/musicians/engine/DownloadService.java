package com.sidm.musicians.engine;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.sidm.musicians.R;
import com.sidm.musicians.db.MusiciansDBHelper;
import com.sidm.musicians.objects.Musician;
import com.sidm.musicians.util.Utils;

import java.io.IOException;
import java.util.List;

/**
 * Created by sidm on 23.04.2016.
 */
public class DownloadService extends IntentService{

    public static final String ACTION_DOWNLOADSERVICE = "com.sidm.musicians.downloadservice";
    public static final String KEY_DOWNLOADSERVICE = "DOWNLOAD_SERVICE_ERROR_DESCR";


    public DownloadService(){
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String response = null;
        List<Musician> musicians = null;
        try{
            //Thread.sleep(5000);
        }catch(Exception e){

        }
        String errorMessage = null;
        if(Utils.isNetworkConnectionAvailable()) {
            try {
                response = NetworkCommunicator.sendRequest(NetworkCommunicator.link);
                musicians = MusiciansJSONParser.parseMusicianData(response);

                MusiciansDBHelper db = MusiciansDBHelper.getInstance(this);
                db.updateMusicianInfo(musicians);
            }catch(IOException e){
                errorMessage = getResources().getString(R.string.error_connection_error);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            errorMessage = getResources().getString(R.string.error_no_connection);
        }

        if(errorMessage != null){
            Intent responseIntent = new Intent(ACTION_DOWNLOADSERVICE);
            responseIntent.putExtra(KEY_DOWNLOADSERVICE, errorMessage);
            LocalBroadcastManager.getInstance(this).sendBroadcast(responseIntent);
        }

    }

}
